# Help and Support Analytics

This repository exists for two main reasons: 1). Open source the data from the
Minds Help and Support group in usable formats, and 2). to analyze queries
in order to drive business decisions.